export function fetchResumeData(url) {
  return fetch(url).then(res => res.json());
}
