import { fetchResumeData } from './api';
import { RESUME_URL } from './consts';

export default class Resume {
  constructor() {
    this.initElement();
  }

  initElement() {
    this.$introWrapperElm = document.getElementsByClassName(
      'introduce-wrapper'
    )[0];
    this.$descElem = document.getElementsByClassName('description')[0];
    this.$eduWrapperElem = document.getElementsByClassName(
      'education-item-wrapper'
    )[0];
  }

  start() {
    this.fetchData();
  }

  fetchData() {
    fetchResumeData(RESUME_URL).then(res => {
      if (res) {
        this.person = res;
        this.render();
      }
    });
  }

  render() {
    this.$introWrapperElm.innerHTML = `<p>
    HELLO,
    </p>
    <p class="introduce">
      MY NAME IS ${this.person.name} ${
      this.person.age
    }YO AND THIS IS MY RESUME/CV
    </p>`;

    this.$descElem.innerHTML = `${this.person.description}`;

    const eduList = this.person.educations.reduce((prev, cur) => {
      prev += `<li class="education-item">
              <div class="left">
                ${cur.year}
              </div>
              <div class="right">
                <h2 class="brief">${cur.title}</h2>
                <p class="detail">
                    ${cur.description}
                </p>
              </div>
              </li>`;
      return prev;
    }, '');

    this.$eduWrapperElem.innerHTML = eduList;
  }
}
